import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import {
    EMPLOYEE_UPDATE,
    EMPLOYEE_CREATE,
    EMPLOYEES_FETCH_SUCCESS,
    EMPLOYEE_SAVE_SUCCESS
} from './type';

export const employeeUpdate = ({ prop, value }) => {
     return { 
         type: EMPLOYEE_UPDATE,
         payload: { prop, value }
     };
};

//   The actions payload will be an object having the key of prop
// and a key value and each of those represent like the prop name
// and the actual value.

export const employeeCreate = ({ name, phone, shift }) => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/employees`)
           .push({ name, phone, shift })
           .then(() => { 
               dispatch({ type: EMPLOYEE_CREATE });
               Actions.employeeList({ type: 'reset' });
        });
    };
};

//   Get access to our firebase database
// and then make a refrence to users/userId/employees
// it's a path to our Json data store as I path through the
// different Json that we have 

// reset argument in  employee list is:
// we don't want to back to previous screen.
// so sfter we create the new user We will
// reset the form and navigate back to the employee list

export const employeesFetch = () => { // fetch a list of employees
    const { currentUser } = firebase.auth();

   return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/employees`)
            .on('value', snapshot => {
                dispatch({ type: EMPLOYEES_FETCH_SUCCESS, payload: snapshot.val() });
            });
    };
};

//  on () => any time any value any data call this function
// with an object to describe the data that's sitting in there ( snapshot)
// it is an object that describe what data is in there.

export const employeeSave = ({ name, phone, shift, uid }) => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
            .set({ name, phone, shift })
            .then(() => {
                dispatch({ type: EMPLOYEE_SAVE_SUCCESS });
                Actions.employeeList({ type: 'reset' });
            });
    };
};

export const employeeDelete = ({ uid }) => {
    const { currentUser } = firebase.auth();

    return () => {
        firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
            .remove()
            .then(() => {
                Actions.employeeList({ type: 'reset' });
            });
    };
};
