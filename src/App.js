import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Router from './Router';

export default class App extends React.Component {
  componentWillMount() {
    const config = {
      apiKey: 'AIzaSyBnh_X4UeGXR_k5mDwyRnGq95OhFyB-5ME',
      authDomain: 'manager-31538.firebaseapp.com',
      databaseURL: 'https://manager-31538.firebaseio.com',
      projectId: 'manager-31538',
      storageBucket: 'manager-31538.appspot.com',
      messagingSenderId: '829953410101'
    };
    firebase.initializeApp(config);
  }

  render() { 
    // REDUX THUNK :
    // using redux thunk to handle asynchronous action creators
    // use to handel any type of asynchronous action creator that 
    // might need to put our code base
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    //    the second argument is for initial state that might want to pass
    // to our redux application
    //    the third argument is what we called stored and answers
    // because it is adding additional functinality to our store   
    return ( 
      <Provider store={store}>
        <Router /> 
      </Provider>
    );
  }
}

//    Router tag is what kind of organizes all the different scenes 
// that we're going to put together for our application.

//    Each scene correspond to one distinct screen that 
// we want to show to the user.
