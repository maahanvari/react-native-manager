// Catch the emailChange actions
// One reducers responsible for all thing authentication
import { 
    EMAIL_CHANGED, 
    PASSWORD_CHANGED,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    LOGIN_USER
 } from '../actions/type';

const INITIAL_STATE = { 
    email: '',
    password: '',
    user: null,
    error: '',
    loading: false
};
 
export default (state = INITIAL_STATE, action) => {
    console.log(action);
    
    switch (action.type) {
        case EMAIL_CHANGED:
            return { ...state, email: action.payload };
        case PASSWORD_CHANGED:
            return { ...state, password: action.payload };
        case LOGIN_USER:
            return { ...state, loading: true, error: '' };
        case LOGIN_USER_SUCCESS:
            return { ...state, ...INITIAL_STATE, user: action.payload };
        case LOGIN_USER_FAIL: 
            return { ...state, error: 'Authentication Failed', loading: false };
        default:
         return state; 
    }
};
// {...state, something }
//make a new object and take all of the properties
// of my existence state object and through them into
// that object then define the property email
//given the value of action.payload an add it to state
// if state has a email property it override by new one
