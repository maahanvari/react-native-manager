import React from 'react';     
import { Scene, Router, Actions } from 'react-native-router-flux';
import LoginForm from './component/LoginForm';
import EmployeeList from './component/EmployeeList';
import EmployeeCreate from './component/EmployeeCreate';
import EmployeeEdit from './component/EmployeeEdit';

const RouterComponent = () => {
    return (
        <Router sceneStyle={{ paddingTop: 65 }}>
            <Scene key="auth">
                <Scene key="login" component={LoginForm} title="Please Login" />
            </Scene>

            <Scene key="main">
                <Scene 
                    onRight={() => Actions.employeeCreate()}
                    rightTitle="Add" 
                    key="employeeList" 
                    component={EmployeeList} 
                    title="Employees " 
                    initial
                /> 
                <Scene 
                    key="employeeCreate"
                    component={EmployeeCreate}
                    title="Employees"
                />
                <Scene
                    key="employeeEdit"
                    component={EmployeeEdit}
                    title="Edit Employee"
                />
            </Scene>
        </Router>
    );  
};

export default RouterComponent;

//   SceneStyle will be applied to all the different scenes 
// on our application when they show up on the screen

//    if you do not place an initial flag it's going to figure out
// which seemed to show first solely based on order
